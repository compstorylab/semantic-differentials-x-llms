from sentence_embeddings import load_words, semantic_embedding
import numpy as np
import pandas as pd
from sklearn.linear_model import Lasso, LinearRegression

fname = '../data/en_VAD_GES_PDS_labMT.tsv'
df = load_words(fname)
embeddings = semantic_embedding(df)

clf = Lasso(alpha=0.1)
clf.fit(embeddings, df['valence'].values)
print(embeddings)
print(df['valence'].values)
print(clf.coef_)
print(clf.intercept_)


# Lasso is estimating zero coefficients for everything.
# Looks like the embedding is non-linear.
# I'm going to pivot to using random forests to estimate feature importance