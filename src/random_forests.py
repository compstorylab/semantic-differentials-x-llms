from sentence_embeddings import load_words, semantic_embedding
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
from matplotlib import colors


def get_embedding_extremes(words, embeddings):
    """given a list of words and embeddings,
     return the max and min word for each dimensions

     Args:
         words: list of str
         embeddings: embedding vectors assumed to be aligned with words

    Returns:
        min and max word (tuple)
     """

    embedding_df = pd.DataFrame(embeddings, index=words)

    min_words, max_words = embedding_df.idxmin().values, embedding_df.idxmax().values
    min_idx, max_idx = embedding_df.idxmin().values, embedding_df.idxmax().values


    labels = [f'{i[0]} - {i[1]}' for i in zip(min_words, max_words)]
    print(labels)
    return np.array(labels)

semantic_differential = 'valence'

fname = '../data/en_VAD_GES_PDS_labMT.tsv'
df = load_words(fname)
embeddings = semantic_embedding(df, model='sentence-transformers/average_word_embeddings_glove.6B.300d')
labels = get_embedding_extremes(df.index, embeddings)

X_train, X_test, y_train, y_test = train_test_split(
embeddings, df[semantic_differential].values, test_size=0.2, random_state=42)

clf = RandomForestRegressor(n_estimators=100, max_depth=None, verbose=1, n_jobs=-1)
clf.fit(X_train,y_train)
print(clf.feature_importances_)



# plotting and exploration

# feature importance plot
sorted_idx = clf.feature_importances_.argsort()[::-1]
f,ax = plt.subplots()
ax.barh(labels[sorted_idx][:20], clf.feature_importances_[sorted_idx][:20])
ax.invert_yaxis()
plt.xlabel("Feature Importance")
plt.title(semantic_differential)
plt.tight_layout()
plt.show()

# show Mean Squared Error
print("Predicting")
prediction = clf.predict(X_test)
# Compute mean squared error
mse = mean_squared_error(y_test, prediction)
# Print results
print(f"MSE: {mse:.4f}")
print(f"MAE: {mse ** 0.5:.4f}")

# Plot scatter plots
f,ax = plt.subplots(3,3,figsize=(15,15))
ax = ax.flatten()
for i in range(9):
    ax[i].plot(embeddings[:,sorted_idx[i]], df[semantic_differential].values, ',', ms=1)
    ax[i].set_xlabel(f'{i}th most important\n embedding feature', fontsize=10)
    ax[i].set_ylabel(f'{semantic_differential}',fontsize=10)
    ax[i].set_title(labels[sorted_idx[i]])
plt.tight_layout()
plt.show()

# plot 2d histograms
f,ax = plt.subplots(3,3,figsize=(15,15))
ax = ax.flatten()
for i in range(9):
    ax[i].hist2d(embeddings[:,sorted_idx[i]], df[semantic_differential].values, bins=80,)
    ax[i].set_xlabel(f'{i}th most important\n embedding feature', fontsize=10)
    ax[i].set_ylabel(f'{semantic_differential}',fontsize=10)
    ax[i].set_title(f"<-- {labels[sorted_idx[i]]} -->")
plt.tight_layout()
plt.show()