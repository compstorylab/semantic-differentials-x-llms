import torch

# Both should return 'True'
print('Is mac m1 gpu available?')
print(torch.backends.mps.is_available())
print('Is pytorch compiled to use m1 gpu?')
print(torch.backends.mps.is_built())
