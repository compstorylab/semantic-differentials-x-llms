import os
from sentence_transformers import SentenceTransformer
import pandas as pd
import umap
import matplotlib.pyplot as plt
import matplotlib.colors as colors

from bokeh.models import ColorBar, LogColorMapper, LinearColorMapper, CategoricalColorMapper, CDSView, GroupFilter, BooleanFilter
from bokeh.plotting import figure, output_file, show, ColumnDataSource
from bokeh.io import show
from bokeh.models import CustomJS, RadioButtonGroup, OpenURL, TapTool, TabPanel, Tabs, Select, TextInput, DateRangeSlider, DateSlider



def load_words(fname='../data/en_VAD_GES_PDS_labMT.tsv'):
    """ Load words with semantic differential scores"""
    df = pd.read_csv(fname, sep='\t')
    df.set_index('word', inplace=True)
    print(df)
    return df


def semantic_embedding(df, text_column=None, model="all-mpnet-base-v2"):
    """ Get semantic embeddings from a model of choice.

    Dataframe should be indexed by words, or a text_column should be passed.

     Potential models include:
      {"all-MiniLM-L6-v2", 'all-MiniLM-L12-v2', "all-mpnet-base-v2", 'sentence-transformers/average_word_embeddings_glove.6B.300d'}

    see more here: https://www.sbert.net/docs/pretrained_models.html
    """

    model = SentenceTransformer(model, device='mps')

    # grab words from index if not passing sentences in a text column
    if text_column is None:
        words = [str(i) for i in df.index.values]

    else:
        words = df[text_column].values

    #Sentences are encoded by calling model.encode()
    embeddings = model.encode(words, show_progress_bar=True)

    return embeddings

def umap_coordinates(df, embeddings):
    print('Running UMAP...')
    umap_result = umap.UMAP(n_neighbors=15, n_components=2, min_dist=0).fit_transform(embeddings)
    print("UMAP complete.")

    df['UMAP_x'] = umap_result[:, 0]
    df['UMAP_y'] = umap_result[:, 1]
    return df

def static_plot(df, embeddings, semantic_diff='valence'):
    """ plot with matplotlib"""

    f,ax = plt.subplots(figsize=(10,10))

    ax.scatter(df['UMAP_x'],df['UMAP_y'],
               c=df[semantic_diff],
               alpha=0.7,
               cmap='PuOr',
               norm=colors.CenteredNorm(),
    )


    ax.axis('off')
    plt.savefig(f'../figures/{semantic_diff}.pdf')
    plt.show()


def grid_plot_PDS(df):
    """ Plot power danger structure within the semantic embeddings"""


    f, ax = plt.subplots(3,3, figsize=(33, 30))
    ax = ax.flatten()
    semantic_diffs = ['valence','arousal','dominance','goodness','energy','structure','power','danger','happiness']
    for i, semantic_diff in enumerate(semantic_diffs):
        cmap = ax[i].scatter(df['UMAP_x'], df['UMAP_y'],
                   s=4,
                   c=df[semantic_diff],
                   alpha=0.7,
                   cmap='PuOr',
                   norm=colors.Normalize(),
                   )
        cbx = f.colorbar(cmap, ax=ax[i], label=semantic_diff)
        cbx.set_label(semantic_diff, fontsize=26)
        cbx.ax.tick_params(labelsize=18)
        ax[i].axis('off')

    plt.tight_layout()

    plt.savefig(f'../figures/grid_plot.pdf')
    plt.savefig(f'../figures/grid_plot.png')

    plt.show()

def interactive_plot(df):
    output_file(f'../figures/interactive/PDS.html')

    color_fields =['valence','arousal','dominance','goodness','energy','structure','power','danger','happiness']
    color_mappers = []

    # add color mappers for tabs
    for field in color_fields:
        color_mappers.append(
                LinearColorMapper(palette="PuOr11",
                                  low=min(df[field]),
                                  high=max(df[field])
                                  )
                            )

    source = ColumnDataSource(df)

    TOOLTIPS = """
               <div>
                   <div>
                       <span style="font-size: 14px;">@word</span>
                   </div>
                </div>
                """


    #initialize tab list
    tabs_list = []
    panels = []

    for i, color_field in enumerate(color_fields):
        if i == 0:
            tabs_list.append(figure(width=700, height=700,
                                    tooltips=TOOLTIPS,
                                    tools="pan,wheel_zoom,box_zoom,reset,tap",
                                    toolbar_location="below",
                                    title=f'Semantic Differentials',
                                    ))
        else:
            tabs_list.append(figure(width=700, height=700,
                                    x_range=tabs_list[0].x_range, y_range=tabs_list[0].y_range,
                                    tooltips=TOOLTIPS,
                                    tools="pan,wheel_zoom,box_zoom,reset,tap",
                                    toolbar_location="below",
                                    title=f'Semantic Differentials',
                                    ))

        tabs_list[i].circle('UMAP_x', 'UMAP_y', source=source,
                            fill_color={'field': color_field, 'transform': color_mappers[i]},
                            fill_alpha=0.7, line_color=None)
        tabs_list[i].xaxis.visible = False
        tabs_list[i].yaxis.visible = False

        color_bar = ColorBar(color_mapper=color_mappers[i], label_standoff=12)
        tabs_list[i].add_layout(color_bar, 'right')

        panels.append(TabPanel(child=tabs_list[i], title=color_fields[i]))
    show(Tabs(tabs=panels))

def main():
    """ """

    # load words
    fname = '../data/en_VAD_GES_PDS_labMT.tsv'
    enriched_fname = fname[:-4] + "+UMAP" + fname[-4:]
    if not os.path.isfile(enriched_fname):
        df = load_words(fname)

        # get embeddings
        embeddings = semantic_embedding(df)
        print(embeddings)

        # dimensionality reduction
        df = umap_coordinates(df, embeddings)

        df.to_csv(enriched_fname, sep='\t')
    else:
        df = load_words(enriched_fname)


    # plot embeddings with semantic differentials
    #static_plot(df, embeddings)

    grid_plot_PDS(df)

    interactive_plot(df)


if __name__ == "__main__":
    main()