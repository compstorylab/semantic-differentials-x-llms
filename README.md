# Semantic Differentials x LLM Embeddings


## Description
An exploration of how semantic differentials for words (and maybe sentences) are represented in large language models

![GridPlot](figures/grid_plot.png)


## Installation
Installation requires a Conda installation
```
# The version of Anaconda may be different depending on when you are installing`
curl -O https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-arm64.sh
sh Miniconda3-latest-MacOSX-arm64.sh
# and follow the prompts. The defaults are generally good.

```

and the following commands:
```
conda create -n llms python=3.11
conda activate llms
conda install pytorch::pytorch torchvision torchaudio -c pytorch
conda install -c huggingface transformers
conda install -c conda-forge umap-learn matplotlib bokeh sentence-transformers
pip install --upgrade --force-reinstall scikit-learn
```

## Usage
The core script is `sentence_embeddings.py` which contains functionality to load text data,
compute sentence embeddings with pre-trained language models, and generate basic plots of words or texts
in a dimensionally reduced embedding space, colored by semantic differential values.

```
conda activate llms
python sentence_embeddings.py
```

To access this core functionality in a python script, 

Further investigations are in the files `lasso.py` and `random_forests.py`.

#### Lasso
Lasso is a linear regression variation that uses L1 regularization to bias coefficents to zero. 
If the semantic embedding space had mostly linear relationships with a given semantic differential within a limited subset of it's dimensions,
Lasso regression should help us identify those dimensions, as the ones with the highest absolute value coefficients.

We found that for typical regularization strengths (alpha=0.1), Lasso finds all coefficients are zero. For very small values of the regularization parameter this changes. 


![Lasso](figures/lasso.png)


#### Random Forest

To avoid the potential issues with assuming linear relationships between semantic embedding and semantic differentials, 
we also use random forest regression. 

We use feature importance to attempt to identify important dimensions within the semantic space. 


